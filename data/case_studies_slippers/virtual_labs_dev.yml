title: Virtual Labs Dev
file_name: vlabsdev
canonical_path: /customers/virtual_labs_dev/
cover_image: /images/blogimages/vlabsdev_coverimage.jpg
cover_title: |
  GitLab enables the VLabsDev community support academic excellence 
cover_description: |
  VLabsDev adopted GitLab for improved collaboration, visibility and security
twitter_image: /images/blogimages/vlabsdev_coverimage.jpg
twitter_text: Learn how GitLab enables the @VLABSIITB community support academic excellence
customer_logo: /images/case_study_logos/vlabsdev_logo.png
customer_logo_css_class: brand-logo-tall
customer_industry: Education
customer_location: India
customer_solution: GitLab Self Managed Ultimate
customer_employees: |
  10,000 (IT students)
customer_overview: |
  VLabsDev is an academic community helping students enhance their education with open source projects.
customer_challenge: |
  A lack of version control and project management within a single application limits collaboration and visibility.  
key_benefits: >-

  
    A single DevOps platform

  
    Integrated CI/CD

  
    Security features

  
    Issue tracking

  
    Increased collaboration and visibility
customer_stats:
  - stat: 3.5 minutes
    label: Deployments down from 30 minutes to 3.5 minutes
  - stat: 150%   
    label: Over 300 GitLab projects, growth of 150% in 3 years
  - stat: 80%
    label: 80% reduction in lead time
customer_study_content:
  - title: the customer
    subtitle: An academic community committed to helping students
    content: >-
    
  
       VLabsDev is a development community created by [Indian Institute of Technology Bombay](https://www.iitb.ac.in). The organization strives to generate community-created content to enable students and instructors from any engineering, science, and polytechnic institute to benefit from hackathons, events, documentation, lab manuals, pedagogy, and storyboards. The VLabsDev academic community uses these best practices to complement their education.

  - title: the challenge
    subtitle:  Lack of project management and version control
    content: >-
    

       In supporting students and instructors, the VLabsDev team encountered a unique challenge. How can contributors with different levels of experience and from various universities collaborate and locate information? As an open source project, VLabsDev had to foster a seamless contributor experience that appealed to anyone - regardless of skill level. Adopting tools that had a steep learning curve could alienate some novice users, while expensive applications could prohibit students from contributing.

        The simplest solution to invite anyone to collaborate was to use a Google Drive, an application available to most universities, to share source codes. As the community grew, VLabsDev shifted to Bitbucket, but the tool proved too difficult for the community and lacked several features. The team then used GitHub, but progress stagnated because of the lack of integrated DevOps features and private repositories which were offered only as a paid feature.

        Realizing that their previous tools weren’t meeting community needs, the IT team at VLabsDev knew it was time to find an effective solution. The IT team assessed their needs and identified two objectives that their ideal solution should meet. One, a tool had to provide version control and a platform for the community to maintain lab source codes. Two, project planning and management features should help community members identify active contributors on projects, analyze lab development speed, set timelines, and track assigned tasks. Dedicated to encouraging the academic community to contribute to documentation and lab manuals, the VLabsDev team sought a new solution to facilitate a seamless contributor experience. 


  - title: the solution
    subtitle:  A DevOps platform meets every user’s needs
    content: >-

        The VLabsDev team began assessing solutions and added several technical requirements to their ideal list of features. Integrated CI/CD, security testing, and a single platform to maintain code repositories topped the list, and the team quickly settled on GitLab as the solution that offered every feature the community and the IT team needed. “GitLab offers everything under one roof. A team doesn’t have to use any other tool or waste time and energy testing other options to get the job done,” explained Pushdeep Mishra, Senior Project Manager.
    

        In the past, VLabsDev struggled with project management. The team used email and folders before briefly switching to BitBucket, but the team found the features limited and complicated. The team then used GitHub but the lack of provisions for private teams for the free version prompted a search for a comprehensive solution. As an open DevOps platform, GitLab offered VLabsDev a comprehensive set of project planning features to support both community contributors and the IT team. “Being open source, GitLab is an excellent DevOps tool, and as a team leader, developing and coordinating with my team is super easy thanks to GitLab’s features like Issues, Epics, Roadmaps, Time Tracking, and Milestones,” shared Shardul Sayankal, Team Leader. Using GitLab’s planning features, faculty members can create epics and issues to organize development plans for experiments and a roadmap to encompass all the experiments for the entire lab. 
    
    
        Amey Satwe, Community Mentor at VLabsDev added “I have been using GitLab for a couple of years. As a maintainer of several projects, I adore the features such as built in CI/CD, Issue templates, Security testing. I could streamline the projects with much ease to our production with the gitlab-ci.yml . It’s the best available git platform”. The built-in security features have made it possible for the team to ship high-quality and secure code. “Before using GitLab, we didn’t have a defined protocol for deploying our build, which led to several security threats identified by the university’s system admins. The built-in container scanning feature has helped us ship more secure code,” said Pushdeep. The team also uses container scanning, SAST, and DAST to assist developers in identifying vulnerabilities while they’re coding. VLabsDev selected GitLab to enable both the academic community and the IT team to collaborate and deliver quickly, and the impact has transformed the development experience. 
       
        
  - title: the results
    subtitle:   Increased collaboration, security, and visibility
    content: >-

        Students, instructors, and IT team members have fully embraced GitLab to create over 300 projects and use features to provide end-to-end visibility of project development. The team has been able to efficiently share code between distributed individuals, which, previously, was a significant challenge given different code versions and various communication threads. The instilled habits of collaborating remotely helped community contributors and the IT team quickly adjust to the COVID-19 lockdown, which forced long-distance collaboration. VLabsDev has been able to modernize its architecture, moving from a monolithic-based architecture to a modularized development and deployment method using Docker images to build, test, and deploy. Previously, the team was dependent on Docker Hub to store and access images, but with GitLab, features are integrated within one platform, and the team appreciates automation, because it allows them to work towards their goals and not worry about manual tasks. Modernizing the architecture has helped the team to improve performance, delivery, and monitoring. 

       
        One of the most impactful ways in which GitLab has helped the team meet urgent needs is when team members collaborated to build the VLabsDev platform. The ability to coordinate work with various teams, such as backend, frontend, testing, and deploying was instrumental in successfully completing the project. GitLab helped the team track bugs and resolve vulnerabilities to ensure that the project was completed on time. The team hopes that the benefits of using GitLab extend beyond VLabsDev. Students are now trained to code, test, build, and deploy using GitLab, and the team hopes that early introduction and adoption to GitLab helps students gain industry-level project development experience. Thereby, increasing their chance of employability. Having mastered the version control, security, project management, and CI/CD features, the team has started to explore and experiment with integrations with Docker Swarm and Kubernetes. “Without GitLab, it would have been difficult for us to build VLabsDev. We would have been a decade behind in progress, and we would’ve struggled with too many tools to streamline CI/CD, security, and project management. GitLab allows us to do our best work.”

        
    
  
        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: It is safe to say that had we not adopted GitLab to better manage collaboration and code amongst the community, there would have been a complete meltdown of our system. GitLab provides a one-stop solution to all DevOps features required for high-performing teams to deliver software
    attribution: Pushdeep Mishra
    attribution_title:  Project Manager, VLabsDev








