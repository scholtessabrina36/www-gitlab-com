---
layout: handbook-page-toc
title: "GitLab Sales Team READMEs"
---

## Sales Team READMEs

- [Noria Aidam's README (Sales Developement Representative Enterprise)](/handbook/sales/readmes/noria_aidam.html)
- [Chris Cowell's README (Senior Professional Services Technical Instructor)](/handbook/sales/readmes/chris-cowell/)
- [Nick Lotz's README (Professional Services Technical Instructor)](/handbook/sales/readmes/nick-lotz/)
- [Tim Poffenbargers's README (Solutions Architect)](/handbook/sales/readmes/tim-poffenbarger.html)
- [Francis Potter's README (Solution Architect)](/handbook/sales/readmes/francis-potter.html)
- [JC Choi's README (Professional Services Technical Instructional Designer)](/handbook/sales/readmes/jc-choi/)
